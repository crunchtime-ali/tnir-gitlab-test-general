A project testing how .gitlab-ci.yml works.


## Check for gitlab-org/gitlab-ce#17276

### SVG

![](https://upload.wikimedia.org/wikipedia/commons/0/02/SVG_logo.svg)

via https://commons.wikimedia.org/wiki/File:SVG_logo.svg

### PNG

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/SVG_logo.svg/240px-SVG_logo.svg.png)

via https://commons.wikimedia.org/wiki/File:SVG_logo.svg